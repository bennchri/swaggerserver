# Serving Swagger

This is a standalone app that exposes a distributed swagger spec on the local file system over an embedded http 
server (jetty). This is done to enable Swagger first design and easy import into WSO2 API Manager 1.7.0. 

Contributors:
Benneth Christiansson

Requirements:
Java Runtime 7

License:
See license.txt

Acknowledgement:
Built using the amazing micro Java web framework Spark.
Kudos to Per Wendel and a big thanks!
http://sparkjava.com/

To use create a swagger specification in local file system that mirrors the resources your API will expose.
For instance you have the api:
```
/petstore
/petstore/pet
/petstore/store
/petstore/user
```
Then in your local file system create folders accordingly:
```
DOC_ROOT/petstore
DOC_ROOT/petstore/pet
DOC_ROOT/petstore/store
DOC_ROOT/petstore/user
```
Then place a file containing the Swagger definition for each resource in corresponding folder, like:
```
DOC_ROOT/petstore/main.json
DOC_ROOT/petstore/pet/pet.json
DOC_ROOT/petstore/store/store.json
DOC_ROOT/petstore/user/user.json
```
Now run the SwaggerServer and pass the swagger-root in this case "DOC_ROOT/petstore" as an argument at runtime.
The app will tell you the url you can use for the import into WSO2 API Manager, like:
```
java -jar SwaggerServer1.0.0.jar ~/docs/petstore
Exposing file: main.json over: http://localhost:4567/petstore
Exposing file: pet.json over: http://localhost:4567/petstore/pet
Exposing file: store.json over: http://localhost:4567/petstore/store
Exposing file: user.json over: http://localhost:4567/petstore/user
== Spark has ignited ...
>> Listening on 0.0.0.0:4567
```